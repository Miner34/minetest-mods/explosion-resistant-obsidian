--Get translators (even if they don't exist)
local S = minetest.get_translator("explosion_resistant_obsidian")

--Load mod files
local default_path = minetest.get_modpath("explosion_resistant_obsidian")

--Create another type of obsidian
minetest.register_node("explosion_resistant_obsidian:obsidian", {
	description = S("Explosion resistant obsidian"),
	tiles = {"default_obsidian.png"},
	sounds = {},
	groups = {cracky = 1, level = 2},
	on_blast = function() end --Make it blast proof
})

--Create blast proof shield item to craft explosion proof obsidian
minetest.register_craftitem("explosion_resistant_obsidian:blast_shield", {
	description = S("Blast shield\nUsed to craft explosion\nresistant obsidian"),
	inventory_image = "blast_shield.png",
	groups = {}
})

local D = "doors:door_wood"
local TD = "doors:trapdoor"

--Add a craft for the blast shield
minetest.register_craft({
	output = "explosion_resistant_obsidian:blast_shield",
	recipe = {
		{"", TD, ""},
		{TD, D, TD},
		{"", TD, ""}
	}
})
--Add two craftings for the explosion resistant obsidian
minetest.register_craft({
	output = "explosion_resistant_obsidian:obsidian",
	recipe = {
		{"explosion_resistant_obsidian:blast_shield", "default:obsidian"}
	}
})

minetest.register_craft({
	output = "explosion_resistant_obsidian:obsidian",
	recipe = {
		{"default:obsidian", "explosion_resistant_obsidian:blast_shield"}
	}
})

